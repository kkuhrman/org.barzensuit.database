/**
 * @file:  bzendbapi.h
 * @brief: Extension C API for org.barzensuit software which access databases.
 *
 * @copyright:	Copyright (C) 2019 Kuhrman Technology Solutions LLC
 * @license:	GPLv3+: GNU GPL version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BZEN_DBAPI_H_
#define __BZEN_DBAPI_H_

#include <config.h>

/********************************************************************************
 * mariadb-connector-c
 *******************************************************************************/

/********************************************************************************
 * Database server connection API
 *******************************************************************************/

/**
 * @typedef bzen_db_handle_t
 */
typedef unsigned int bzen_db_handle_t;

/**
 * Prepare, initialize and open a connection to the database given by parameters.
 *
 * @param const char* host    Host name or IP address.
 * @param const char* user    Database server user name.
 * @param const char* pass    Database server user's password.
 * @param const char* db      Default database to be used when performing queries.
 * @param unsigned int port   Port number on which database server listens.
 * @param const char* sock    UNIX socket or named pipe that should be used.
 * @param unsigned long flags Flags which set various connection options.
 *
 * @see https://mariadb.com/kb/en/library/mysql_real_connect/
 *
 * @return bzen_db_handle_t Handle to database connection or NULL on failure.
 */
bzen_db_handle_t bzen_db_open(
  const char* host,
  const char* user,
  const char* pass,
  const char* db,
  unsigned int port,
  const char* sock,
  unsigned long flags
  
);

#endif /* __BZEN_DBAPI_H_ */